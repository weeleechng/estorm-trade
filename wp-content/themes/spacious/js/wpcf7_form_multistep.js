function scroll_to_top (element)
{
  var dest = jQuery(element).offset().top;

  if (jQuery(document).scrollTop() != dest)
  {
    jQuery('html,body').animate (
    {
      scrollTop: dest,
    }, 
    500);
  }
}

function wpcf7__prevalidate_form (clicked_element)
{

  var invalidcount = 0;
  jQuery(clicked_element).parents ("div.form-page").find ("input.wpcf7-validates-as-required, select.wpcf7-validates-as-required").each (function (index, item)
  {
    if (jQuery(item).attr ("aria-required") == "true" && jQuery(item).val() == "")
    {
      invalidcount++;
      jQuery(item).addClass ("wpcf7-not-valid");
      jQuery(item).attr ("aria-invalid", "true");
    }
    else
    {
      jQuery(item).removeClass ("wpcf7-not-valid"); 
      jQuery(item).attr ("aria-invalid", "false");
    }
  });

  if (invalidcount > 0)
  {
    return false;
  }
  else
  {
    return true;
  }
}

function wpcf7__change_validate ()
{
  jQuery("div.form-page").find ("input.wpcf7-validates-as-required, select.wpcf7-validates-as-required").each (function (index, item)
  {
    jQuery(item).change (function ()
    {
      if (jQuery(this).val () == "")
      {
        jQuery(this).addClass ("wpcf7-not-valid");
        jQuery(this).attr ("aria-invalid", "true");
      }
      else
      {
        jQuery(this).removeClass ("wpcf7-not-valid");
        jQuery(this).attr ("aria-invalid", "false");
      }
    });
  });
}

function wpcf7_form_multipage_setup ()
{
  wpcf7__change_validate ();
  jQuery._form_multipage_count = 0;

  jQuery('form').each (function (form_index, form_item)
  {
    if (jQuery(form_item).find ('div.form-page').length > 1)
    {
      jQuery(form_item).find ('div.form-page').addClass ('hidden');
      /* display first page: */
      jQuery(form_item).find ('div.form-page:eq(' + jQuery._form_multipage_count + ')').removeClass ('hidden');

      jQuery(form_item).find ('a#form-next').removeAttr ('href').attr ({ 'data':'questionnaire-next', });
      jQuery(form_item).find ('a#form-prev').removeAttr ('href').attr ({ 'data':'questionnaire-prev', });

      jQuery(form_item).find ('a#form-next').on ('click.form_multistep', function (clickevent)
      {
        clickevent.preventDefault ();
        if (wpcf7__prevalidate_form (jQuery (this)) == true)
        {
          if (jQuery._form_multipage_count < (jQuery(form_item).find ('div.form-page').length - 1))
          jQuery._form_multipage_count++;
          jQuery(form_item).find ('div.form-page').addClass ('hidden');
          jQuery(form_item).find ('div.form-page:eq(' + jQuery._form_multipage_count + ')').removeClass ('hidden');
          scroll_to_top (jQuery (form_item));          
        }
        else
        {
          alert ("Oops, please ensure you have filled in all required fields before you proceed.");
        }
      });

      jQuery (form_item).find ('a#form-prev').on ('click.form_multistep', function (clickevent)
      {
        clickevent.preventDefault ();
        if (jQuery._form_multipage_count > 0)
        {
          jQuery._form_multipage_count--;
          jQuery(form_item).find ('div.form-page').addClass ('hidden');
          jQuery(form_item).find ('div.form-page:eq(' + jQuery._form_multipage_count + ')').removeClass ('hidden');
          scroll_to_top (jQuery(form_item));
        }
      });

    }

  });

}

jQuery(document).ready (function ()
{ 
  wpcf7_form_multipage_setup ();
});
