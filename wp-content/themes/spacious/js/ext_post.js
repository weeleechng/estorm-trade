function wpcf7_post_form ()
{
  if (jQuery('form.wpcf7-form').length > 0 && !jQuery('form.wpcf7-form').hasClass('invalid'))
  {
    console.log ("Initiating form post");
    jQuery.ajax
    ({
      url     : 'http://leads.estorm.com/lead/entry',
      type    : 'POST',
      dataType: 'json',
      data    : jQuery('form.wpcf7-form').serialize (),
      success : function (data) { console.log ("Form post complete"); },
      error   : function (xhr, err) { console.log ("Error submitting form"); }
    });
  }

}