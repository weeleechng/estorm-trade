(function ($) {

function wpcf7_form_single_submit_setup ()
{
	$("form.wpcf7-form").on ("submit.single", function ()
	{
		$("form.wpcf7-form input[type='submit'], form.wpcf7-form button[type='submit']").attr ({ "disabled" : true });
	});

	$(document).ajaxComplete (function (event, xhr, settings)
	{
		$("form.wpcf7-form input[type='submit']").removeAttr ("disabled");
	});
}

$(document).ready (function ()
{ 
  wpcf7_form_single_submit_setup (); 
});

})(jQuery);
