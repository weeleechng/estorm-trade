<?php

wp_enqueue_style ('storm_landstrip_style', SPACIOUS_CSS_URL. '/landing-strip.css');

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="http://resources.estorm.com/icon/favicon.png" type="image/x-icon" />
<link rel="apple-touch-icon" href="http://resources.estorm.com/icon/favicon.png"/>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
</head>
<body class="storm-front">
	<div class="front-nav">
		<div class="nav-content">
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<h1 class="menu-toggle"><?php _e( 'Menu', 'spacious' ); ?></h1>
				<?php
					if ( has_nav_menu( 'primary' ) ) {									
						wp_nav_menu( array( 'theme_location' => 'primary' ) );
					}
					else {
						wp_page_menu();
					}
				?>
			</nav>
			<div class="nav-right">
				<?php the_widget ("WP_Widget_Search"); ?>
			</div>
		</div>
	</div>
	<div id="page">
		<div id="main">
			<!--div class="inner-wrap"-->

<!-- START THE MAGIC HERE -->

				<div class="landing-container">
					<div id="logo" class="row logo full">
						<img class="img-logo" alt="estorm Export Trading" title="estorm Export Trading" src="<?php echo get_template_directory_uri (); ?>/css/img/logo.png" ></img>
					</div>
						
					<div class="landing-content">

						<div class="vertical-link main-row"></div>
						
						<div id="row-who" class="row fullwidth">
							<div class="content inline left">
								<h1>Who we are</h1>
							</div>
							<div id="icon-who" class="icon inline"></div>
							<div class="content inline right">
								<p>estorm export trading (EET) provides advisory services, consultancy, procurement and assists qualified foreign buyers to obtain export financing from export credit agencies (ECA) in the United States.</p>
							</div>
						</div>

						<div class="vertical-link main-row"></div>

						<div id="row-goal" class="row fullwidth">
							<div class="content inline left">
								<h1>Our goal</h1>
							</div>
							<div id="icon-goal" class="icon inline"></div>
							<div class="content inline right">
								<p>To facilitate trade between foreign buyers and the US. Any importer regardless of their size is restricted in the amount of trade they can do by the following few things:</p>
							</div>
						</div>

						<div class="vertical-link sub-row"></div>
					
						<div id="row-upfront" class="subrow right">
							<div class="subrow-container">
								<div class="link"></div>
								<div id="icon-upfront" class="icon inline"></div>
								<div class="content inline">
									<p>Having to pay upfront for merchandise coming from the US (since most US Manufacturers/Suppliers won't trade unless paid 30-50% upfront)</p>
								</div>
							</div>
						</div>

						<div class="vertical-link sub-row"></div>
					
						<div id="row-cashflow" class="subrow left">
							<div class="subrow-container">
								<div class="link"></div>
								<div id="icon-cashflow" class="icon inline"></div>
								<div class="content inline">
									<p>Having the cashflow to order more</p>
								</div>
							</div>
						</div>

						<div class="vertical-link sub-row"></div>
					
						<div id="row-bank" class="subrow right">
							<div class="subrow-container">
								<div class="link"></div>
								<div id="icon-bank" class="icon inline"></div>
								<div class="content inline">
									<p>Having to go to a bank or secondary markets in your country to get loans at high interests of 10%-20%</p>
								</div>
							</div>
						</div>

						<div class="vertical-link sub-row"></div>

						<div id="row-barrier" class="subrow left">
							<div class="subrow-container">
								<div class="link"></div>
								<div id="icon-barrier" class="icon inline"></div>
								<div class="content inline">
									<p>Not knowing how to source products in the US due to language/culture barriers</p>
								</div>
							</div>
						</div>

						<div class="vertical-link resolve-row"></div>

						<div id="row-resolve" class="row fullwidth padded">
							<div class="content">
								<h1>EET can resolve all of these issues</h1>
							</div>
						</div>

						<div class="vertical-link resolve-row-content"></div>
					
						<div id="row-resolve-1-2" class="resolverow full">
							<div class="horizontal-link"></div>
							<div class="content left">
								<div id="icon-resolve-1" class="icon resolve"></div>
								<p>We work with the US supplier, and through the export credit agencies we work with, we guarantee payment to the US supplier.</p>
							</div>
							<div class="vertical-link resolve-row-content mobile-only"></div>
							<div class="content right">
								<div id="icon-resolve-2" class="icon resolve"></div>
								<p>We can ship the products and finance the remaining 85%.</p>
							</div>
							<br />
							<div class="vertical-link resolve-row-content"></div>
							<div class="content middle">
								<div id="icon-resolve-3" class="icon resolve"></div>
								<p>We can provide a foreign importer of US products a credit facility and all they need is to be able to raise an irrevocable letter of credit for 15% which is only due when the merchandise arrives in your country.</p>
							</div>
						</div>
					
					</div>

					<div class="landing-footer">
						<div class="content left">
							<h2>Why Should You Consider estorm export trading?</h2>
						</div>
						<div class="content right">
							<p>Through the approved US export trading agencies we work with, we are able to obtain financing at very competitive interest rates.</p>
							<h3>As low as 5%-6%!</h3>
							<p>These rates are much lower than any commercial bank in Europe, Asia or Africa can offer. In cases where the loan amount is $10 million or more, we may be able to obtain rates that are even lower.</p>
						</div>
					</div>

				</div>

<!-- END THE MAGIC HERE -->

			</div>
		<!--/div-->
		<footer id="colophon" class="clearfix">
			<?php get_sidebar( 'footer' ); ?>
			<div class="footer-socket-wrapper clearfix">
				<div class="inner-wrap">
					<div class="footer-socket-area">
						Copyright &copy; 1999-<?php print date ("Y"); ?> e-storm Export Trading Co.
						<nav class="small-menu" class="clearfix"><?php 
if (has_nav_menu('footer')) 
{
	wp_nav_menu (array ( 
		'theme_location' => 'footer',
		'depth' => -1,
	));
} 
					?></nav>
					</div>
				</div>
			</div>			
		</footer>
	<a href="#masthead" id="scroll-up"></a>
	</div>
	<?php wp_footer(); ?>
</body>
</html>