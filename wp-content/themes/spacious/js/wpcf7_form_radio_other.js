(function ($) {

function get_other (element)
{
  return jQuery ('.' + jQuery (element).attr ('name') + '-other');
}

function wpcf7_form_radio_other_setup ()
{
  jQuery ('input[type=radio][value^=other], input[type=radio][value^=Other]').each (function (index, item)
  {
    jQuery (get_other (item)).css ({ 'display': 'none', });
    
    jQuery ('input[type=radio][name=' + jQuery(item).attr ('name') + ']').on ('click.check', function () 
    {
      if (jQuery (item).is (':checked'))
      {
        jQuery (get_other (item)).css ({ 'display':'', });
      }
      else
      {
        jQuery (get_other (item)).css ({ 'display':'none', });
      }

    });
  });

}

jQuery (document).ready (function ()
{ 
  wpcf7_form_radio_other_setup (); 
});

})(jQuery);
